﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Drawing;

namespace Sittisak.Io.Barcode.Tests
{
    [TestClass]
    public class HelpersTests
    {
        [TestMethod]
        public void GetBarcode()
        {
            //Example calculate a barcode position
            //Document Width: 12.5
            //- Barcode Position X: 7.5 => ~60
            //- Barcode Width: 4 => ~32

            //Document Height: 17.7
            //- Barcode Position Y: 0.5 => ~2
            //- Barcode Height: 1 => ~7

            ////Code2
            //Document Width: 12.5
            //- Barcode Position X: 3.5 => ~28
            //- Barcode Width: 4 => ~32

            //Document Height: 17.7
            //- Barcode Position Y: 1.8 => ~10
            //- Barcode Height: 1 => ~7

            IList<BarcodeLocation> barcodeLocations = new List<BarcodeLocation>()
            {
                new BarcodeLocation("Code", new Point(60, 2), new Size(32, 7)),
                new BarcodeLocation("Code2", new Point(28, 10), new Size(32, 7))
            };

            Helpers.GetBarcode(@"data.pdf", FileFormat.PDF, ref barcodeLocations);

            Assert.AreEqual(barcodeLocations[0].Barcode, "591108896");
            Assert.AreEqual(barcodeLocations[0].BarcodeFormat, "CODE_39");

            //Cannot scan the barcode
            //Assert.AreEqual(barcodeLocations[1].Barcode, "1450007000");
            //Assert.AreEqual(barcodeLocations[1].BarcodeFormat, "CODE_39");
        }
    }
}
