﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sittisak.Io.Barcode
{
    public class BarcodeLocation
    {
        public string Name { get; private set; }
        public string Barcode { get; set; }
        public string BarcodeFormat { get; set; }

        public Point Location { get; private set; }
        public Size Size { get; private set; }

        public BarcodeLocation(string name, Point location, Size size)
        {
            this.Name = name;
            this.Location = location;
            this.Size = size;
        }

        public Rectangle GetBarcodeArea(Image image)
        {
            int x = Convert.ToInt32(Math.Floor(image.Width * (this.Location.X / 100.0)));
            int y = Convert.ToInt32(Math.Floor(image.Height * (this.Location.Y / 100.0)));
            int width = Convert.ToInt32(Math.Floor(image.Width * (this.Size.Width / 100.0)));
            int height = Convert.ToInt32(Math.Floor(image.Height * (this.Size.Height / 100.0)));

            return new Rectangle(x, y, width, height);
        }
    }
}
