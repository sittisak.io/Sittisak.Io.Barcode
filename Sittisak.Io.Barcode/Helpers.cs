﻿using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;

namespace Sittisak.Io.Barcode
{
    public static class Helpers
    {
        public static void GetBarcode(string sourcePath, FileFormat fileFormat , ref IList<BarcodeLocation> barcodeLocations)
        {
            if (fileFormat == FileFormat.PDF)
            {
                Image image = null;

                using (var pdfDocument = PdfDocument.Load(sourcePath))
                {
                    var pageSize = pdfDocument.PageSizes.FirstOrDefault();

                    if (pageSize == null)
                        throw new Exception("Page is empty.");

                    int scale = 2;
                    image = pdfDocument.Render(0, Convert.ToInt32(pageSize.Width * scale), Convert.ToInt32(pageSize.Height * scale), 96, 96, true);
                }

                if (image == null)
                    throw new Exception("Cannot load PDF file.");

                var barcodeReader = new BarcodeReader();

                foreach (var barcodeLocation in barcodeLocations)
                {
                    barcodeLocation.Barcode = "";

                    var barcodeImage = CropImage(image, barcodeLocation.GetBarcodeArea(image));

                    //Save Barcode to file for test.
                    //barcodeImage.Save($"{barcodeLocation.Name}-output.png", ImageFormat.Png);

                    var barcodeResult = barcodeReader.Decode(barcodeImage as Bitmap);
                    barcodeLocation.Barcode = barcodeResult?.Text;
                    barcodeLocation.BarcodeFormat = barcodeResult?.BarcodeFormat.ToString();
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private static Image CropImage(Image image, Rectangle cropArea)
        {
            Bitmap bitmap = new Bitmap(image);
            return bitmap.Clone(cropArea, bitmap.PixelFormat);
        }
    }
}
